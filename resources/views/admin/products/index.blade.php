@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <a href="{{ route('products.create') }}" class="btn btn-primary">New Product</a>
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>SKU</th>
                                <th>Category</th>
                                <th>inStock</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ $p->sku }}</td>
                                    <td>{{ $p->category->name }}</td>
                                    <td>
                                        @if($p->instock)
                                            <span class="btn btn-xs btn-success">Yes</span>
                                        @else
                                            <span class="btn btn-xs btn-danger">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('products.edit', $p->id) }}" class="btn btn-default btn-xs">Edit</a>
                                        {{ Form::open(['route' => ['products.destroy', $p->id], 'class' => 'form-delete', 'method' => 'DELETE']) }}
                                        <button class="btn btn-xs btn-danger">Delete</button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $(function(){
            $('.form-delete').submit(function(e){
                if(!confirm('Are you sure to delete this item?')){
                    e.preventDefault();
                }
            });
        })
    </script>
@stop
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        {{ Form::open(['route' => 'products.store']) }}
                        <div class="form-group">
                            <label for="">Name</label>
                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Provider Name']) }}
                        </div>
                        <div class="form-group">
                            <label for="">SKU</label>
                            {{ Form::text('sku', old('sku'), ['class' => 'form-control', 'placeholder' => 'SKU']) }}
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'rows' => 4]) }}
                        </div>
                        <div class="form-group">
                            <label for="">Category</label>
                            {{ Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control']) }}
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="instock" {{ ($product->instock) ? 'checked' : '' }}> InStock
                            </label>
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

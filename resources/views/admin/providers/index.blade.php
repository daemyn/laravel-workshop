@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <a href="{{ route('providers.create') }}" class="btn btn-primary">New Provider</a>
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($providers as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ $p->address }}</td>
                                    <td>
                                        <a href="{{ route('providers.edit', $p->id) }}" class="btn btn-default btn-xs">Edit</a>
                                        {{ Form::open(['route' => ['providers.destroy', $p->id], 'class' => 'form-delete', 'method' => 'DELETE']) }}
                                        <button class="btn btn-xs btn-danger">Delete</button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    <script>
        $(function(){
            $('.form-delete').submit(function(e){
                if(!confirm('Are you sure to delete this item?')){
                    e.preventDefault();
                }
            });
        })
    </script>
@stop
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $title }}</div>
                    <div class="panel-body">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p>{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        {{ Form::open(['route' => ['providers.update', $provider->id], 'method' => 'PUT']) }}
                        <div class="form-group">
                            <label for="">Name</label>
                            {{ Form::text('name', $provider->name, ['class' => 'form-control', 'placeholder' => 'Provider Name']) }}
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            {{ Form::text('address', $provider->address, ['class' => 'form-control', 'placeholder' => 'Provider Name']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop